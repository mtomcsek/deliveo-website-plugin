<?php

$countries = [];
$countries['ad'] = 'Andorra';
$countries['al'] = 'Shqipëri';
$countries['at'] = 'Österreich';
$countries['ba'] = 'Босна и Херцеговина';
$countries['be'] = 'België';
$countries['bg'] = 'България';
$countries['by'] = 'Bielaruś';
$countries['ch'] = 'Schweiz';
$countries['cy'] = 'Κύπρος';
$countries['cz'] = 'Česko';
$countries['de'] = 'Deutschland';
$countries['dk'] = 'Dánia';
$countries['ee'] = 'Eesti';
$countries['es'] = 'España';
$countries['fi'] = 'Suomi';
$countries['fr'] = 'France';
$countries['gb'] = 'United Kingdom';
$countries['gr'] = 'Ελλάδα';
$countries['hr'] = 'Hrvatska';
$countries['hu'] = 'Magyarország';
$countries['ie'] = 'Ireland';
$countries['is'] = 'Ísland';
$countries['it'] = 'Italia';
$countries['kv'] = 'Косово';
$countries['li'] = 'Liechtenstein';
$countries['lt'] = 'Lietuva';
$countries['lu'] = 'Luxemburg';
$countries['lv'] = 'Latvija';
$countries['mc'] = 'Monaco';
$countries['md'] = 'Moldova';
$countries['me'] = 'Црна Гора';
$countries['mk'] = 'Северна Македонија';
$countries['mt'] = 'Malta';
$countries['nl'] = 'Nederland';
$countries['no'] = 'Norge';
$countries['pl'] = 'Polska';
$countries['pt'] = 'Portugal';
$countries['ro'] = 'România';
$countries['rs'] = 'Србија';
$countries['se'] = 'Sverige';
$countries['sm'] = 'San Marino';
$countries['sk'] = 'Slovensko';
$countries['st'] = 'Slovenija';
$countries['ua'] = 'Україна';
$countries['va'] = 'Città del Vaticano';
