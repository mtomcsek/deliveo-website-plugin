<?php

final class Lang {

    private $trans = null;
    private $acceptLang = ['hu', 'en', 'de', 'gr'];
    private $deliveoApiLangCodes = ['en' => 'en_EN', 'hu' => 'hu_HU', 'de' => 'de_DE', 'gr' => 'gr_GR'];
    private $langTexts = ['en' => 'English', 'hu' => 'Magyar', 'de' => 'Deutsche', 'gr' => 'Ελληνικά'];

    /**
     * Call this method to get singleton
     *
     * @return Lang
     */
    public static function instance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new Lang();
        }
        return $inst;
    }

    /**
     * Private v so nobody else can instantiate it
     *
     */
    private function __construct() {
        if (!isset($_SESSION['lang'])) {
            $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            $_SESSION['lang'] = in_array($lang, $this->acceptLang) ? $lang : 'en';
        }

        if (file_exists(__DIR__ . '/' . filter_var($_SESSION['lang']) . '.json')) {
            $this->trans = json_decode(file_get_contents(__DIR__ . '/' . filter_var($_SESSION['lang']) . '.json'), true);
        }
    }

    public function _l($text) {
        if ($this->trans && array_key_exists($text, $this->trans) && $this->trans[$text] != '') {
            return $this->trans[$text];
        } else {
            return $text;
        }
    }

    public function getAcceptLang() {
        return $this->acceptLang;
    }

    public function getdeliveoApiLangCodes() {
        return $this->deliveoApiLangCodes;
    }
    public function getlangText($langcode) {
        return (array_key_exists($langcode,  $this->langTexts))? $this->langTexts[$langcode]: '';
    }

}
