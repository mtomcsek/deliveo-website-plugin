<?php

ini_set('display_errors', 0);

if (version_compare(PHP_VERSION, '5.5.0') < 0) {
    die(PHP_VERSION . ' >= 5.5 php version required.');
}
if (!extension_loaded('zlib')) {
    die('zlib php modul is required!');
}
session_start();
require_once __DIR__ . '/lang/lang.php';
// nyelv beállítása nyelvállasztás után
if (!is_null(filter_input(INPUT_GET, 'lang'))) {
    $_SESSION['lang'] = in_array(filter_input(INPUT_GET, 'lang'), Lang::instance()->getAcceptLang()) ? filter_input(INPUT_GET, 'lang') : 'en';
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/deliveo.php';
require_once __DIR__ . '/countries.php';

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

// check env variable
if (empty(getenv('API_KEY'))) {
    die(Lang::instance()->_l('Be kell állítnai az API_KEY env változót') . '!');
}
if (empty(getenv('LICENCE'))) {
    die(Lang::instance()->_l('Be kell állítnai az LICENCE env változót') . '!');
}
if (empty(getenv('BASE_URL'))) {
    die(Lang::instance()->_l('Be kell állítnai az BASE_URL env változót') . '!');
}
if (!in_array('curl', get_loaded_extensions())) {
    die(Lang::instance()->_l('curl-t engedélyezni kell a szerveren') . '!');
}
if (getenv('PAYLIKE_PAYMENT') == '1' && (getenv('BARION_PAYMENT') == '1')) {
    die(Lang::instance()->_l('Egyszerre csak egy fizetési szolgáltatót használhatsz') . '!');
}
if (getenv('BARION_PAYMENT') == '1' && (!getenv('BARION_POS_KEY') || !getenv('BARION_ACCOUNT_EMAIL'))) {
    die(Lang::instance()->_l('BARION_POS_KEY és BARION_ACCOUNT_EMAIL megadása kötelező') . '!');
}
if (getenv('PAYLIKE_PAYMENT') == '1' && (!getenv('PAYLIKE_KEY'))) {
    die(Lang::instance()->_l('PAYLIKE_KEY megadása kötelező') . '!');
}

$deliveoApi = new Deliveo();
// API kulcs ellenőrzése
if (!$deliveoApi->validKey()) {
    die(Lang::instance()->_l('Érvénytelen api kulcs') . '!');
}

$deliveryOptions = $deliveoApi->getDelivery();
$requiredFields  = $deliveoApi->required();

// aktuális tab beállítása
$tab = filter_input(INPUT_GET, 'tab');
if (is_null($tab)) {
    $tab = getenv('SEND_ON') ? 'send-tab' : 'getpackage-tab';
}

if ($tab === 'getpackage-tab') {
    $message = filter_input(INPUT_GET, 'msg');
    $groupId = filter_input(INPUT_GET, 'groupId');
}

function url($url)
{
    return getenv('BASE_URL') . $url;
}
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php echo getenv('APP_NAME'); ?></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo url('/plugins/intl-tel-input-master/build/css/intlTelInput.css'); ?>">

	<style>
		.hide {
			display: none;
		}
		.service_price{
			display: inline-block;
			margin: 5px 0 0 20px;
		}
		.btn-success{
			float: left;
		}
		.barion_logo{
			text-align: center;
			padding: 20px 0 0 0;
		}
		.packages_summary{
			margin-bottom: 15px;
		}
	</style>
</head>

<body>

	<?php
if (isset($_SESSION['input_data'])) {
    $input_data = json_decode($_SESSION['input_data']);
}
if ($_SESSION['payment_error']) {
    ?>
			<script>alert(<?php echo $_SESSION['payment_error']; ?>);</script>
			<?php
$_SESSION['payment_error'] = null;
}
?>

	<!-- nav -->
	<nav class="navbar navbar-light bg-light navbar-expand-lg" style="">
		<a class="navbar-brand mr-0 mr-md-2" href="<?php echo url(''); ?>" aria-label="Bootstrap">
			<img src="<?php echo url('/img/Deliveo_logo_64_trBG.png'); ?>" alt="">
		</a>
		<?php echo getenv('APP_NAME'); ?>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse ml-3" id="navbarSupportedContent">

			<ul class="nav nav-pills">
				<?php if (getenv('SEND_ON')) {?>
					<li class="nav-item">
						<a class="nav-link <?php echo ($tab === 'send-tab') ? 'active' : ''; ?>" href="<?php echo url('/?tab=send-tab'); ?>">
							<?php echo Lang::instance()->_l('Feladás'); ?>
						</a>
					</li>
				<?php }?>
				<?php if (getenv('GET_PACKAGEDATA_ON')) {?>
					<li class="nav-item">
						<a class="nav-link <?php echo ($tab === 'getpackage-tab') ? 'active' : ''; ?>" href="<?php echo url('/?tab=getpackage-tab'); ?>">
							<?php echo Lang::instance()->_l('Követés'); ?>
						</a>
					</li>
				<?php }?>

				<div class="dropdown">
					<button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<img src="<?php echo url('/img/' . filter_var($_SESSION['lang']) . '.png'); ?>" height="16" width="16">
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
						<?php foreach (Lang::instance()->getAcceptLang() as $langItem) {?>
							<a class="dropdown-item" href="<?php echo url('/?lang=' . $langItem); ?>">
								<img src="<?php echo url('/img/' . $langItem . '.png'); ?>" height="16" width="16">
								<?php echo Lang::instance()->getlangText($langItem); ?>
							</a>
						<?php }?>
					</div>
				</div>
			</ul>
		</div>
	</nav>
	<!-- nav vége -->
	<div class="container">
		<!-- tabok -->
		<div class="tab-content" id="pills-tabContent">

			<!-- csomag feladás tab -->
			<?php if ($tab === 'send-tab' && getenv('SEND_ON')) {
    ?>
				<div id="pills-send">

					<div class="tab-pane active" id="send-tab" role="tabpanel" aria-labelledby="send-tab">
						<div class="card">
							<div class="card-header bg-primary text-white">
								<?php echo Lang::instance()->_l('Feladás') ?>
							</div>
							<div class="card-body">
								<div class="alert alert-success hide">
								</div>
								<div class="row">
									<div class="col-lg-8">
<?php
$form_action = url('/ajax.php?action=send');
    if (getenv('BARION_PAYMENT')) {
        $form_action = url('/ajax.php?action=barion_payment');
    }
?>
										<form method="POST" action="<?php echo $form_action; ?>" id="send-form">

											<!-- Feladó -->
											<h2><?php echo Lang::instance()->_l('Feladó'); ?></h2>

											<div class="form-group">
												<label for="sender">
													<?php echo Lang::instance()->_l('Feladó'); ?> 
													<?php echo Lang::instance()->_l('név'); ?> 
													<?php echo (array_key_exists('sender', $requiredFields) ? '*' : ''); ?></label>
												<input type="text" name="sender" class="form-control" maxlength="25" <?php echo (array_key_exists('sender', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender)) {echo 'value="' . $input_data->sender . '"';}?> >
											</div>

											<div class="form-row">
												<div class="form-group col-lg-4">
													<label for="sender_country">
														<?php echo Lang::instance()->_l('Ország'); ?> 
														<?php echo (array_key_exists('sender_country', $requiredFields) ? '*' : ''); ?></label>
													<select name="sender_country" class="form-control" <?php echo (array_key_exists('sender_country', $requiredFields) ? 'required' : ''); ?>>

														<?php foreach ($countries as $value => $name) {?>
															<option <?php if (isset($input_data->sender_country) && $input_data->sender_country == $value) {echo 'selected';}?> value="<?php echo $value; ?>"><?php echo $name; ?></option>
														<?php } ?>

													</select>
												</div>
												<div class="form-group col-lg-2">
													<label for="sender_zip">
														<?php echo Lang::instance()->_l('Irányítószám'); ?> 
														<?php echo (array_key_exists('sender_zip', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="sender_zip" class="form-control" maxlength="15" <?php echo (array_key_exists('sender_zip', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_zip)) {echo 'value="' . $input_data->sender_zip . '"';}?> >
												</div>
												<div class="form-group col-lg-6">
													<label for="sender_city">
														<?php echo Lang::instance()->_l('Település'); ?> 
														<?php echo (array_key_exists('sender_city', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="sender_city" class="form-control" maxlength="50" <?php echo (array_key_exists('sender_city', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_city)) {echo 'value="' . $input_data->sender_city . '"';}?> >
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-lg-6">
													<label for="sender_address">
														<?php echo Lang::instance()->_l('Utca, házszám'); ?> 
														<?php echo (array_key_exists('sender_address', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="sender_address" class="form-control" maxlength="50" <?php echo (array_key_exists('sender_address', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_address)) {echo 'value="' . $input_data->sender_address . '"';}?> >
												</div>
												<div class="form-group col-lg-6">
													<label for="sender_apartment">
														<?php echo Lang::instance()->_l('Épület, emelet, ajtó'); ?> 
														<?php echo (array_key_exists('sender_apartment', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="sender_apartment" class="form-control" maxlength="50" <?php echo (array_key_exists('sender_apartment', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_apartment)) {echo 'value="' . $input_data->sender_apartment . '"';}?> >
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-lg-6" id="sender_phone">
													<label for="sender_phone">
														<?php echo Lang::instance()->_l('Telefonszám'); ?> 
														<?php echo (array_key_exists('sender_phone', $requiredFields) ? '*' : ''); ?></label>
													<input title="eg.: +36201234567" placeholder="+3611234567" type="tel" name="sender_phone" class="form-control" maxlength="25" <?php echo (array_key_exists('sender_phone', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_phone)) {echo 'value="' . $input_data->sender_phone . '"';}?> >

													<div class="hide text-success valid-msg">✓ Érvényes</div>
													<div class="hide text-danger error-msg"></div>
												</div>
												<div class="form-group col-lg-6">
													<label for="sender_email">Email
														<?php echo (array_key_exists('sender_email', $requiredFields) ? '*' : ''); ?></label>
													<input type="email" name="sender_email" class="form-control" maxlength="50" <?php echo (array_key_exists('sender_email', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_email)) {echo 'value="' . $input_data->sender_email . '"';}?> >
												</div>
											</div>
											<!-- Feladó -->

											<hr>
											<!-- Címzett -->
											<h2><?php echo Lang::instance()->_l('Címzett'); ?></h2>
											<div class="form-group">
												<label for="consignee">
													<?php echo Lang::instance()->_l('Címzett'); ?> 
													<?php echo Lang::instance()->_l('név'); ?> 
													<?php echo (array_key_exists('consignee', $requiredFields) ? '*' : ''); ?></label>
												<input type="text" name="consignee" class="form-control" maxlength="25" <?php echo (array_key_exists('consignee', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee)) {echo 'value="' . $input_data->consignee . '"';}?> >
											</div>
											<div class="form-row">
												<div class="form-group col-lg-4">
													<label for="consignee_country">
														<?php echo Lang::instance()->_l('Ország'); ?> 
														<?php echo (array_key_exists('consignee_country', $requiredFields) ? '*' : ''); ?></label>
													<select name="consignee_country" class="form-control" <?php echo (array_key_exists('consignee_country', $requiredFields) ? 'required' : ''); ?>>
														<?php foreach ($countries as $value => $name) {?>
															<option <?php if (isset($input_data->consignee_country) && $input_data->consignee_country == $value) {echo 'selected';}?> value="<?php echo $value; ?>"><?php echo $name; ?></option>
														<?php }?>
													</select>
												</div>
												<div class="form-group col-lg-2">
													<label for="consignee_zip">
														<?php echo Lang::instance()->_l('Irányítószám'); ?> 
														<?php echo (array_key_exists('consignee_zip', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="consignee_zip" class="form-control" maxlength="15" <?php echo (array_key_exists('consignee_zip', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_zip)) {echo 'value="' . $input_data->consignee_zip . '"';}?> >
												</div>

												<div class="form-group col-lg-6">
													<label for="consignee_city">
														<?php echo Lang::instance()->_l('Település'); ?> 
														<?php echo (array_key_exists('consignee_city', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="consignee_city" class="form-control" maxlength="50" <?php echo (array_key_exists('consignee_city', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_city)) {echo 'value="' . $input_data->consignee_city . '"';}?> >
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-lg-6">
													<label for="consignee_address">
														<?php echo Lang::instance()->_l('Utca, házszám'); ?> 
														<?php echo (array_key_exists('sender', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="consignee_address" class="form-control" maxlength="50" <?php echo (array_key_exists('consignee_address', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_address)) {echo 'value="' . $input_data->consignee_address . '"';}?> >
												</div>
												<div class="form-group col-lg-6">
													<label for="consignee_apartment">
														<?php echo Lang::instance()->_l('Épület, emelet, ajtó'); ?> 
														<?php echo (array_key_exists('consignee_apartment', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="consignee_apartment" class="form-control" maxlength="50" <?php echo (array_key_exists('consignee_apartment', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_apartment)) {echo 'value="' . $input_data->consignee_apartment . '"';}?> >
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-lg-6" id="consignee_phone">
													<label for="consignee_phone">
														<?php echo Lang::instance()->_l('Telefonszám'); ?> 
														<?php echo (array_key_exists('consignee_phone', $requiredFields) ? '*' : ''); ?></label>
													<input title="eg.: +36201234567" placeholder="+3611234567" type="tel" name="consignee_phone" class="form-control" maxlength="25" <?php echo (array_key_exists('consignee_phone', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_phone)) {echo 'value="' . $input_data->consignee_phone . '"';}?> >
													<div class="hide text-success valid-msg">✓ Valid</div>
													<div class="hide text-danger error-msg"></div>
												</div>
												<div class="form-group col-lg-6">
													<label for="consignee_email">Email 
														<?php echo (array_key_exists('consignee_email', $requiredFields) ? '*' : ''); ?></label>
													<input type="email" name="consignee_email" class="form-control" maxlength="50" <?php echo (array_key_exists('consignee_email', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_email)) {echo 'value="' . $input_data->consignee_email . '"';}?> >
												</div>
											</div>
											<!-- Címzett vége -->
											<hr>

											<!-- Számlafizető -->
											<h2><?php echo Lang::instance()->_l('Számlafizető'); ?></h2>
											<div class="form-group">
												<input type="radio" name="customer_data" value="1" checked=""> <?php echo Lang::instance()->_l('Megegyezik a feladóval'); ?><br>
												<input type="radio" name="customer_data" value="2"> <?php echo Lang::instance()->_l('Megegyezik a címzettel'); ?><br>
												<input type="radio" name="customer_data" value="0"> <?php echo Lang::instance()->_l('Megadom az adatokat'); ?>
											</div>
											<div class="form-group">
												<label for="customer">
													<?php echo Lang::instance()->_l('Számlafizető'); ?> 
													<?php echo Lang::instance()->_l('név'); ?> 
													<?php echo (array_key_exists('customer', $requiredFields) ? '*' : ''); ?></label>
												<input type="text" name="customer" class="form-control" maxlength="25" <?php echo (array_key_exists('customer', $requiredFields) ? 'required' : ''); ?> >
											</div>
											<div class="form-row">
												<div class="form-group col-lg-4">
													<label for="customer_country">
														<?php echo Lang::instance()->_l('Ország'); ?> 
														<?php echo (array_key_exists('customer_country', $requiredFields) ? '*' : ''); ?></label>
													<select name="customer_country" class="form-control" <?php echo (array_key_exists('customer_country', $requiredFields) ? 'required' : ''); ?>>
														<?php foreach ($countries as $value => $name) {?>
															<option value="<?php echo $value; ?>"><?php echo $name; ?></option>
														<?php }?>
													</select>
												</div>
												<div class="form-group col-lg-2">
													<label for="customer_zip">
														<?php echo Lang::instance()->_l('Irányítószám'); ?> 
														<?php echo (array_key_exists('customer_zip', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="customer_zip" class="form-control" maxlength="15" <?php echo (array_key_exists('customer_zip', $requiredFields) ? 'required' : ''); ?> >
												</div>

												<div class="form-group col-lg-6">
													<label for="customer_city">
														<?php echo Lang::instance()->_l('Település'); ?> 
														<?php echo (array_key_exists('customer_city', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="customer_city" class="form-control" maxlength="50" <?php echo (array_key_exists('customer_city', $requiredFields) ? 'required' : ''); ?> >
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-lg-6">
													<label for="customer_address">
														<?php echo Lang::instance()->_l('Utca, házszám'); ?> 
														<?php echo (array_key_exists('customer_address', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="customer_address" class="form-control" maxlength="50" <?php echo (array_key_exists('customer_address', $requiredFields) ? 'required' : ''); ?> >
												</div>
												<div class="form-group col-lg-6">
													<label for="customer_apartment">
														<?php echo Lang::instance()->_l('Épület, emelet, ajtó'); ?> 
														<?php echo (array_key_exists('customer_apartment', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="customer_apartment" class="form-control" maxlength="50" <?php echo (array_key_exists('customer_apartment', $requiredFields) ? 'required' : ''); ?> >
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-lg-6" id="customer_phone">
													<label for="customer_phone">
														<?php echo Lang::instance()->_l('Telefonszám'); ?> 
														<?php echo (array_key_exists('customer_phone', $requiredFields) ? '*' : ''); ?></label>
													<input title="eg.: +36201234567" placeholder="+3611234567" type="tel" name="customer_phone" class="form-control" maxlength="25" <?php echo (array_key_exists('customer_phone', $requiredFields) ? 'required' : ''); ?> >
													<div class="hide text-success valid-msg">✓ Valid</div>
													<div class="hide text-danger error-msg"></div>
												</div>
												<div class="form-group col-lg-6">
													<label for="customer_email">Email<?php echo (array_key_exists('customer_email', $requiredFields) ? '*' : ''); ?></label>
													<input type="email" name="customer_email" class="form-control" maxlength="50" <?php echo (array_key_exists('customer_email', $requiredFields) ? 'required' : ''); ?> >
												</div>
											</div>
											<!-- Számlafizető vége -->
											<hr>

											<!-- További adatok -->
											<h2><?php echo Lang::instance()->_l('További adatok'); ?></h2>
											<div class="form-row">
												<div class="col-lg-12">
													<div class="form-group">
														<label for="delivery">
															<?php echo Lang::instance()->_l('Szállítási opció'); ?> 
															<?php echo (array_key_exists('delivery', $requiredFields) ? '*' : ''); ?></label>
														<select name="delivery" class="form-control" <?php echo (array_key_exists('delivery', $requiredFields) ? 'required' : ''); ?>>
															<?php foreach ($deliveryOptions as $value => $name) {?>
																<option <?php if (isset($input_data->delivery) && $input_data->delivery == $value) {echo 'selected';}?> value="<?php echo $value; ?>"><?php echo $name; ?></option>
															<?php }?>
														</select>
													</div>
													<div class="form-group">
														<label for="insurance">
															<?php echo Lang::instance()->_l('Biztosítás összege'); ?> 
															<?php echo (array_key_exists('insurance', $requiredFields) ? '*' : ''); ?></label>
														<input type="text" name="insurance" class="form-control" <?php echo (array_key_exists('insurance', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->insurance)) {echo 'value="' . $input_data->insurance . '"';}?>>
													</div>
													<div class="form-row">
														<div class="form-group col-lg-6">
															<label for="referenceid">
																<?php echo Lang::instance()->_l('Hivatkozási szám okmánykezelés esetén'); ?> 
																<?php echo (array_key_exists('referenceid', $requiredFields) ? '*' : ''); ?></label>
															<input type="text" name="referenceid" class="form-control" maxlength="25" <?php echo (array_key_exists('referenceid', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->referenceid)) {echo 'value="' . $input_data->referenceid . '"';}?> >
														</div>
														<div class="form-group col-lg-6">
															<label for="tracking">
																<?php echo Lang::instance()->_l('Követőkód'); ?> 
																<?php echo (array_key_exists('tracking', $requiredFields) ? '*' : ''); ?></label>
															<input type="text" name="tracking" class="form-control" maxlength="25" <?php echo (array_key_exists('tracking', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->tracking)) {echo 'value="' . $input_data->tracking . '"';}?>>
														</div>
													</div>

													<div class="form-group">
														<label for="cod">
															<?php echo Lang::instance()->_l('Utánvét összege'); ?></label>

														<div class="input-group mb-3">
															<input type="text" name="cod" class="form-control" <?php echo (array_key_exists('cod', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->cod)) {echo 'value="' . $input_data->cod . '"';} else {echo "value='0'";}?>>
															<div class="input-group-append">
																<span class="input-group-text" id="cod">Ft</span>
															</div>
														</div>
													</div>
												</div>

												<div class="col-lg-12">
													<div class="form-group">
														<label for="comment">
															<?php echo Lang::instance()->_l('Megjegyzés'); ?> 
															<?php echo (array_key_exists('comment', $requiredFields) ? '*' : ''); ?></label>
														<textarea name="comment" class="form-control" maxlength="255" rows="2">
															<?php if (isset($input_data->customer_email)) {echo $input_data->comment;}?></textarea>
													</div>
													<div class="form-group form-check">
														<input <?php if (isset($input_data->priority)) {echo 'checked';}?> name="priority" type="checkbox" value="1" class="form-check-input" id="priority">
														<label class="form-check-label" for="priority">
															<?php echo Lang::instance()->_l('Elsőbbségi kézbesítés'); ?> </label>
													</div>
													<div class="form-group form-check">
														<input name="saturday" <?php if (isset($input_data->saturday)) {echo 'checked';}?> type="checkbox" value="1" class="form-check-input" id="saturday">
														<label class="form-check-label" for="saturday">
															<?php echo Lang::instance()->_l('Szombati kézbesítés'); ?> </label>
													</div>
												</div>
											</div>
											<!-- További adatok vége -->

											<!-- Csomagok -->
											<h4><?php echo Lang::instance()->_l('Csomagok'); ?></h4>

											<div class="table-responsive">
												<table class="table table-bordered table-sm" id="packages">
													<thead>
														<tr>
															<th scope="col"><?php echo Lang::instance()->_l('Súly (kg)'); ?> <?php echo (array_key_exists('weight', $requiredFields) ? '*' : ''); ?></th>
															<th scope="col"><?php echo Lang::instance()->_l('Szélesség (cm)'); ?> <?php echo (array_key_exists('x', $requiredFields) ? '*' : ''); ?></th>
															<th scope="col"><?php echo Lang::instance()->_l('Magasság (cm)'); ?> <?php echo (array_key_exists('y', $requiredFields) ? '*' : ''); ?></th>
															<th scope="col"><?php echo Lang::instance()->_l('Mélysége (cm)'); ?> <?php echo (array_key_exists('z', $requiredFields) ? '*' : ''); ?></th>
															<th scope="col"><?php echo Lang::instance()->_l('Csomag egyedi azonosítója'); ?> <?php echo (array_key_exists('customcode', $requiredFields) ? '*' : ''); ?></th>
															<th scope="col"></th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td class="form-group"><input type="number" min="1" class="packages_weight form-control" name="packages[0][weight]" <?php echo (array_key_exists('weight', $requiredFields) ? 'required' : ''); ?>></td>
															<td class="form-group"><input type="number" min="1" class="packages_x form-control" name="packages[0][x]" <?php echo (array_key_exists('x', $requiredFields) ? 'required' : ''); ?>></td>
															<td class="form-group"><input type="number" min="1" class="packages_y form-control" name="packages[0][y]" <?php echo (array_key_exists('y', $requiredFields) ? 'required' : ''); ?>></td>
															<td class="form-group"><input type="number" min="1" class="packages_z form-control" name="packages[0][z]" <?php echo (array_key_exists('z', $requiredFields) ? 'required' : ''); ?>></td>
															<td class="form-group"><input type="text" class="form-control" name="packages[0][customcode]" maxlength="30" /></td>
															<td><button class="btn btn-warning remove-item" type="button">x</button></td>
														</tr>
													</tbody>
												</table>
											</div>

											<div class="packages_summary">
												<?php echo Lang::instance()->_l('Összsúly'); ?>: <span class="weight_summary">-</span> kg, <?php echo Lang::instance()->_l('össztérfogat'); ?>: <span class="volume_summary">-</span> cm3, <?php echo Lang::instance()->_l('térfogatsúly'); ?>: <span class="dim_summary">-</span>
											</div>

											<button type="button" class="btn btn-success" id="add-package-row"> + <?php echo Lang::instance()->_l('Csomag hozzáadása'); ?></button>

											<div class="service_price">
												<h4></h4>
												<input type="hidden" name="price_to_pay" class="price_to_pay" value="0">
												<input type="hidden" name="currency" class="currency" value="HUF">
											</div>
											<hr>
											<!-- Csomagok vége -->
											<!-- Captcha -->
											<div class="form-group">
												<label>Captcha</label>
												<span id="captcha-info" class="info"></span><br />
												<div class="input-group">
													<div class="input-group-prepend">
														<img id="captcha_code" src="<?php echo url('/captcha_code.php'); ?>" />
													</div>
													<input type="text" class="form-control captcha" name="captcha" id="captcha" required="">
													<div class="input-group-append">
														<button class="btn btn-secondary" type="button" id="captcha-refresh">
															<i class="fas fa-sync"></i>
														</button>
													</div>
												</div>
											</div>
											<!-- Captcha vége -->

											<div class="form-group">
												<button type="<?php if (!getenv('PAYLIKE_PAYMENT')) {?>submit<?php } else {?>button<?php }?>" class="btn btn-primary btn-lg btn-block <?php if (getenv('PAYLIKE_PAYMENT')) {?>paylike_btn<?php }?>"><?php
if (getenv("BARION_PAYMENT") || getenv("PAYLIKE_PAYMENT")) {
        echo Lang::instance()->_l('Tovább a fizetéshez');
    } else {
        echo Lang::instance()->_l('Küldés');
    }
    ?></button>
												<?php if (getenv("BARION_PAYMENT")) {?>
													<div class="barion_logo">
														<img src="/img/barion_300px.png" alt="barion" />
													</div>
												<?php }if (getenv("PAYLIKE_PAYMENT")) {?>
													<div class="barion_logo">
														<img width="200" src="/img/paylike_logo.png" alt="paylike" />
													</div>
												<?php }?>
											</div>
										</form>
									</div>
									<div class="col-lg-4">
										<div class="alert alert-danger" style="display: none;">
											<p><strong><?php echo Lang::instance()->_l('Hiba'); ?>:</strong></p>
											<ul id="error-msg"></ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php }?>
			<!-- csomag feladás tab vége -->

			<!-- csomag napló lekérdezés tab -->
			<?php if ($tab === 'getpackage-tab' && getenv('GET_PACKAGEDATA_ON')) {?>
				<?php $_SESSION['input_data'] = null;?>
				<div id="pills-getpackage">

					<div class="tab-pane" id="get-packagedata-tab" role="tabpanel" aria-labelledby="get-packagedata-tab">
						<div class="card">
							<div class="card-header bg-primary text-white"><?php echo Lang::instance()->_l('Csomag adatok lekérése'); ?></div>
							<div class="card-body">
								<?php if (isset($message) && !is_null($message)) {?>
									<div class="alert alert-success"><?php echo $message; ?></div>
								<?php }?>
								<form method="POST" action="<?php echo url('/ajax.php?action=get'); ?>" id="get-form">
									<div class="row">

										<div class="col-lg-6">
											<div class="form-group">
												<label for="group_id"><?php echo Lang::instance()->_l('Csoportkód'); ?></label>
												<input name="group_id" type="text" value="<?php echo (isset($groupId) && !is_null($groupId)) ? $groupId : ''; ?>" class="form-control" id="group_id" required="">
											</div>
										</div>
										<div class="col-lg-4">
											<label>Captcha</label>
											<span id="captcha-info" class="info"></span><br />
											<div class="input-group">
												<div class="input-group-prepend">
													<img id="captcha_code" src="<?php echo url('/captcha_code.php'); ?>" />
												</div>
												<input type="text" class="form-control captcha" name="captcha" id="captcha" value="">
												<div class="input-group-append">
													<button class="btn btn-secondary" type="button" id="captcha-refresh">
														<i class="fas fa-sync"></i>
													</button>
												</div>
											</div>

										</div>
										<div class="col-lg-2">
											<button type="submit" class="btn btn-primary btn-block" style="margin-top: 32px;">
												OK</button>
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="card hide" id="package-data">
							<div class="card-header bg-primary text-white"><?php echo Lang::instance()->_l('Csomag adatok'); ?></div>
							<div class="card-body">
								<ul id="package-log-result" class="list-group"></ul>

								<div class="row">
									<div class="col-lg-8 ">
										<?php if(getenv('TRACKING_SHOW_DATA') == null || getenv('TRACKING_SHOW_DATA') == 1) { ?>
										<h5 class="text-center mt-2"><?php echo Lang::instance()->_l('Feladó'); ?></h5>
										<ul id="sender-data" class="list-group"></ul>
										<h5 class="text-center mt-2"><?php echo Lang::instance()->_l('Címzett'); ?></h5>
										<ul id="consignee-data" class="list-group"></ul>
										<?php } ?>

										<table class="table" id="package-items">
											<thead>
												<tr>
													<th scope="col"><?php echo Lang::instance()->_l('Súly (kg)'); ?> </th>
													<th scope="col"><?php echo Lang::instance()->_l('Szélesség (cm)'); ?></th>
													<th scope="col"><?php echo Lang::instance()->_l('Magasság (cm)'); ?></th>
													<th scope="col"><?php echo Lang::instance()->_l('Mélysége (cm)'); ?></th>
													<th scope="col"><?php echo Lang::instance()->_l('Csomag egyedi azonosítója'); ?> </th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
									<div class="col-lg-4 mt-2">
										<div class="my-4" id="label">
											<a href="#" target="_blank" class="btn btn-outline-primary btn-block hide">
												<i class="fas fa-file-pdf"></i> <?php echo Lang::instance()->_l('Csomagcímke letöltése'); ?></a>
										</div>
										<?php if (getenv('DOWNLOAD_SIGNATURE_ENABLED')) {?>
											<div class="my-4" id="signature">
												<a href="#" target="_blank" class="btn btn-outline-primary btn-block hide">
													<i class="fas fa-file-pdf"></i> <?php echo Lang::instance()->_l('Aláíráslap letöltése'); ?></a>
											</div>
										<?php }?>
										<?php if (getenv('DISPLAY_TRACKINGURL')) {?>
											<div class="my-4" id="trackingurl">
												<a href="#" target="_blank" class="hide">
													<i class="fas fa-map-marked-alt fa-3x"></i> </a>
											</div>
										<?php }?>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			<?php }?>
			<!-- csomag napló lekérdezés tab vége -->
		</div>
		<!-- tabok vége -->
	</div>

	<!-- assets -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="<?php echo url('/plugins/intl-tel-input-master/build/js/intlTelInput.js'); ?>"></script>
	<script>
		<?php if ($tab === 'send-tab' && getenv('SEND_ON')) {?>
			class intlTel {
				constructor(parentSelector) {
					this.intlTelInputOptions = {
						// allowDropdown: false,
						// autoHideDialCode: false,
						// autoPlaceholder: "off",
						// dropdownContainer: document.body,
						// excludeCountries: ["us"],
						// formatOnDisplay: false,
						geoIpLookup: function(callback) {
							$.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
								var countryCode = (resp && resp.country) ? resp.country : "";
								callback(countryCode);
							});
						},
						// hiddenInput: "full_number",
						initialCountry: "auto",
						// localizedCountries: { 'de': 'Deutschland' },
						// nationalMode: false,
						//onlyCountries: ['hu', 'en'],
						// placeholderNumberType: "MOBILE",
						preferredCountries: ['hu'],
						// separateDialCode: true,
						utilsScript: "<?php echo url('/plugins/intl-tel-input-master/build/js/utils.js'); ?>",
					};
					this.errorMap = [
						"<?php echo Lang::instance()->_l('Érvénytelen szám'); ?>",
						"<?php echo Lang::instance()->_l('Érvénytelen ország kód'); ?>",
						"<?php echo Lang::instance()->_l('Túl rövid'); ?>",
						"<?php echo Lang::instance()->_l('Túl hosszú'); ?>",
						"<?php echo Lang::instance()->_l('Érvénytelen szám'); ?>"
					];
					this.input = document.querySelector(parentSelector + ' input[type=tel');
					this.errorMsg = document.querySelector(parentSelector + ' .error-msg');
					this.validMsg = document.querySelector(parentSelector + ' .valid-msg');
					this.iti = null;
				}

				init() {
					this.iti = window.intlTelInput(this.input, this.intlTelInputOptions);
				}

				reset() {
					this.input.classList.remove("error");
					this.errorMsg.innerHTML = "";
					this.errorMsg.classList.add("hide");
					this.validMsg.classList.add("hide");
				}

				setValidate() {
					this.input.addEventListener('blur', (event) => {
						this.reset();

						if (this.input.value.trim()) {
							if (this.iti.isValidNumber()) {
								this.validMsg.classList.remove("hide");
							} else {
								this.input.classList.add("error");
								var errorCode = this.iti.getValidationError();
								this.errorMsg.innerHTML = this.errorMap[errorCode];
								this.errorMsg.classList.remove("hide");
							}
						}
					});

					// on keyup / change flag: reset
					this.input.addEventListener('change', (event) => {
						this.reset();
					});
					this.input.addEventListener('keyup', (event) => {
						this.reset();
					});
				}
			}

			var intlTel1 = new intlTel('#sender_phone');
			intlTel1.init();
			intlTel1.setValidate();

			var intlTel2 = new intlTel('#consignee_phone');
			intlTel2.init();
			intlTel2.setValidate();

			var intlTel3 = new intlTel('#customer_phone');
			intlTel3.init();
			intlTel3.setValidate();
		<?php }?>

		/*
		 * csomag sorok input tömb indexek újra rendezése
		 */
		var replaceInputArrayIndex = function(inputNameAttribute, newIndex) {
			var start = inputNameAttribute.indexOf('[');
			var end = inputNameAttribute.indexOf(']');

			var inputArrayIndex = inputNameAttribute.substring(start + 1, end);
			return inputNameAttribute.replace('[' + inputArrayIndex + ']', '[' + newIndex + ']');
		}
		var reOrderInputArrayIndex = function(selector) {
			$(selector).each((index, element) => {
				$(element).find('input, textarea').each((labelIndex, inputElemet) => {
					$(inputElemet).attr('name', this.replaceInputArrayIndex($(inputElemet).attr('name'), index));

				});
			});
		}

		function refreshCaptcha() {
			var d = new Date();
			$("#captcha_code").attr('src', 'captcha_code.php?v=' + d.getTime());
		}

		$(document).ready(function() {
			var senderInputs = ['sender', 'sender_zip', 'sender_city', 'sender_address', 'sender_apartment', 'sender_email', 'sender_phone'];
			var consigneeInputs = ['consignee', 'consignee_zip', 'consignee_city', 'consignee_address', 'consignee_apartment', 'consignee_email', 'consignee_phone'];
			var customerInputs = ['customer', 'customer_zip', 'customer_city', 'customer_address', 'customer_apartment', 'customer_email', 'customer_phone'];

			<?php if (isset($input_data)) {?>
				// sender_country,
				$.each(senderInputs, function(index, name) {
					$('input[name="' + customerInputs[index] + '"]').val($('input[name="' + name + '"]').val());
				});

				$('select[name="customer_country"]').val($('select[name="sender_country"] option:selected').val());

				$.ajax({
					method: "POST",
					url: "<?php echo url('/ajax.php?action=getQuote'); ?>",
					data: $('#send-form').serialize(),
					dataType: "json"
				}).done(function(response) {
					if(response.type == "success"){
						$(".service_price h4").html(response.data.quote + " " + response.data.currency);
						$(".service_price .price_to_pay").val(response.data.quote);
						$(".service_price .currency").val(response.data.currency);
						<?php if (getenv('BARION_PAYMENT')) {?>
							if(response.data.quote > 0){
								$("#send-form").attr('action', '<?php echo url('/ajax.php?action=barion_payment'); ?>');
							}
						<?php }?>
					} else {
						$(".service_price h4").html('???');
						$(".service_price .price_to_pay").val(0);
						$(".service_price .currency").val("HUF");
						<?php if (getenv('BARION_PAYMENT')) {?>
							$("#send-form").attr('action', '<?php echo url('/ajax.php?action=send'); ?>');
						<?php }?>
					}
				});

			<?php } else { ?>

				var userLang = navigator.language || navigator.userLanguage;

				$('select[name="sender_country"]').val(userLang.split('-')[0]);
				$('select[name="consignee_country"]').val(userLang.split('-')[0]);
				$('select[name="customer_country"]').val(userLang.split('-')[0]);

			<?php } ?>

			$('input:radio[name="customer_data"]').on('change', function() {
				var selected = $(this).val();

				if (selected == 1) {
					// sender_country,
					$.each(senderInputs, function(index, name) {
						$('input[name="' + customerInputs[index] + '"]').val($('input[name="' + name + '"]').val());
					});

					$('select[name="customer_country"]').val($('select[name="sender_country"] option:selected').val());
				} else if (selected == 2) {
					$.each(consigneeInputs, function(index, name) {
						$('input[name="' + customerInputs[index] + '"]').val($('input[name="' + name + '"]').val());
					});
					$('select[name="customer_country"]').val($('select[name="consignee_country"] option:selected').val());

				} else if (selected == 0) {

					$.each(customerInputs, function(index, name) {
						$('input[name="' + name + '"]').val('');
					});
					$('select[name="customer_country"]').val('hu');
				}
			});

			$('input[name^="sender"]:not(input[name="sender"])').on('keyup', function() {
				if ($('input:radio[name="customer_data"]:checked').val() == 1) {
					var inputName = $(this).attr('name');
					var customerInputName = inputName.replace("sender", "customer");
					$('input[name="' + customerInputName + '"]').val($(this).val());
				}
			});
			$('input[name="sender"]').on('keyup', function() {
				if ($('input:radio[name="customer_data"]:checked').val() == 1) {
					$('input[name="customer"]').val($('input[name="sender"]').val());
				}
			});
			$('input[name^="consignee"]:not(input[name="consignee"])').on('keyup', function() {
				if ($('input:radio[name="customer_data"]:checked').val() == 2) {
					var inputName = $(this).attr('name');
					var customerInputName = inputName.replace("consignee", "customer");
					$('input[name="' + customerInputName + '"]').val($(this).val());
				}
			});
			$('input[name="consignee"]').on('keyup', function() {
				if ($('input:radio[name="customer_data"]:checked').val() == 2) {
					$('input[name="customer"]').val($('input[name="consignee"]').val());
				}
			});

			// Csomaghoz tartozó módosítások status értékeihez rendelt szövegek és ikonok (lásd api dokumentáció: 9. Napló lekérése)
			var status = {
				'rogzitve': {
					'text': 'Rögzítve',
					'icon': 'far fa-check-square fa-2x text-muted'
				},
				'feladva': {
					'text': 'Feladva',
					'icon': 'fas fa-cubes fa-2x text-info'
				},
				'rendszamhoz_rendel': {
					'text': 'Redszámhoz rendelt',
					'icon': 'fas fa-truck-loading fa-2x text-info'
				},
				'futar_felvette': {
					'text': 'Futár felvette',
					'icon': 'fas fa-truck fa-2x text-primary'
				},
				'sikeres': {
					'text': 'Sikeres kézbesítés',
					'icon': 'fas fa-people-carry fa-2x text-success'
				},
				'kezbesitesi_kiserlet': {
					'text': 'Kézbesítési kisérlet',
					'icon': 'fas fa-exclamation-triangle fa-2x text-warning'
				}
			};
			// csomag sor hozzáadás
			$('#add-package-row').on('click', (event) => {
				var packageInputRow = $('#packages tbody tr:first').clone();
				$(packageInputRow).find('input').val('');
				let newRow = $(packageInputRow);
				newRow.appendTo("#packages tbody");
				reOrderInputArrayIndex('#packages tbody tr');
			});
			// csomag sor törlés
			$(document).on('click', '#packages button.remove-item', (event) => {
				if ($('#packages tbody tr').length > 1) {
					$(event.currentTarget).closest('tr').remove();
					calculateSummary();
					reOrderInputArrayIndex('#packages tbody tr');
				}
			});
			// csomag szám paraméterek 0-nál nagyobbak ellenőrzés
			$(document).on('blur', '#packages input[type=number]', (event) => {
				if ($(event.currentTarget).val() <= 0) {
					$(event.currentTarget).val('');
				}
			});

			$('#captcha-refresh').on('click', (event) => {
				event.preventDefault();
				refreshCaptcha();
			});
			// csomag feladás űrlap küldése
			$('#send-form').on('submit', (event) => {
				event.preventDefault();
				// captcha ellenőrzés küldés elött
				$.ajax({
					method: "POST",
					url: "<?php echo url('/ajax.php?action=checkcaptcha'); ?>",
					data: {
						captcha: $('#captcha').val()
					},
					dataType: "json"
				}).done(function(response) {

					if ('success' in response) {
						<?php if (!getenv("PAYLIKE_PAYMENT")) {?>
						var confirmed = confirm('<?php echo getenv('MSG_CONFIRM_SEND'); ?>');
						if (confirmed) {
						<?php }?>
							$.ajax({
								method: "POST",
								url: $(event.currentTarget).attr('action'),
								data: $(event.currentTarget).serialize(),
								dataType: "json",
								beforeSend: function() {
									$('#send-form :submit').prop('disabled', true).append(' <i class="fas fa-sync fa-spin"></i> ');
								}
							}).done(function(data) {

								$('#send-form :submit').prop('disabled', false);
								$('#send-form :submit').find('i').remove();

								if ('errors' in data) {
									$('#error-msg').html('');
									$('.alert.alert-danger').show();
									$.each(data.errors, function(index, message) {
										$('#error-msg').append('<li>' + message + '</li>');
									});
								} else if (data.error_code == 0 && data.type == 'success') {
									$('#send-form :input').val('');
									$('#error-msg').html('');
									$('.alert.alert-danger').hide();
									if(typeof data.data !== 'undefined'){
										$('.alert.alert-success').show();
										$('.alert.alert-success').html('<p>' + data.msg + '</p> <p>' + data.data[0] + '</p>');
									}

									<?php if (getenv('BARION_PAYMENT')) {?>
										if(data.type == "success"){
											window.location.replace(data.barion_url);
										} else {
											alert(data.msg);
										}
									<?php } else {?>
										window.location.replace('/?tab=getpackage-tab&groupId=' + data.data[0] + '&msg=' + data.msg);
									<?php }?>

								} else if (data.error_code == 4012) {
									alert(data.msg + ': ' + data.field);
								} else if (data.error_code > 0) {
									alert(data.msg);
								} else if (data.error_msg) {
									alert(data.error_msg);
								}
							});
						<?php if (!getenv("PAYLIKE_PAYMENT")) {?>
						}
						<?php }?>

					} else {
						alert(response.error);
					}
				});
			});

			var calculateSummary = function() {
				//tömeg számítása
				var total_weight = 0;
				$( $('.packages_weight') ).each(function() {
					if(parseInt($(this).val()) > 0){
						total_weight += parseInt($(this).val());
					}
				});

				$('.weight_summary').html(total_weight);

				//térfogat számítása
				var total_volume = 0;
				var total_dim = 0;

				$( $('.packages_x') ).each(function(index_x) {
					if(parseInt($(this).val()) > 0){

						var x = parseInt($(this).val());
						var y = 0;
						$( $('.packages_y') ).each(function(index_y) {
							if(index_x == index_y){
								if(parseInt($(this).val()) > 0){
									y = parseInt($(this).val());
								}
							}
						});
						var z = 0;
						$( $('.packages_z') ).each(function(index_z) {
							if(index_x == index_z){
								if(parseInt($(this).val()) > 0){
									z = parseInt($(this).val());
								}
							}
						});
						var current_volume = (x*y*z);
						total_volume += current_volume;
						total_dim += (current_volume/5000);
					}
				});

				$('.volume_summary').html(total_volume);

				$('.dim_summary').html(total_dim.toFixed(2));
			}


			var getPackageData = function() {
				$.ajax({
					method: "POST",
					url: $('#get-form').attr('action'),
					data: $('#get-form').serialize(),
					dataType: "json",
					beforeSend: function() {
						$('#get-form :submit').append(' <i class="fas fa-sync fa-spin"></i> ');
					}
				}).done(function(response) {
					$('#get-form :submit i').remove();
					$('#package-data').show();

					if (response.packageLog.error_code == 0 && response.packageLog.type == 'success') {

						if (response.packageLog.data.length == 0) {
							alert('<?php echo getenv('MSG_PACKAGE_NOT_FOUND'); ?>');
						} else {

							$.each(response.packageLog.data, function(index, item) {
								var date = new Date(item.timestamp * 1000);
								var date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + (date.getDate())).slice(-2) +
									' ' + ('0' + (date.getHours())).slice(-2) + ':' + ('0' + (date.getMinutes())).slice(-2);

								var itemView = '<li class="list-group-item"><div class="row">';

								if (item.status in status) {
									itemView += '<div class="col-1"><i class="' + status[item.status].icon + ' mr-2"></i> </div>';
									itemView += '<div class="col-2">' + status[item.status].text + '</div>';
								} else {
									itemView += '<div class="col-3">' + item.status + ' </div>';
								}

								itemView += '<div class="col-3">' + item.status_text + '</div>';
								itemView += '<div class="col-3">' + date + '</div>';
								itemView += '<div class="col-3">' + item.user + '</div>';
								itemView += '</div></li>';

								$('#package-log-result').append(itemView);
							});

							if ($('#signature').length) {

								$('#signature a').show().attr('href', '/signature.php?groupId=' + $('#get-form input[name="group_id"]').val());
							}
							if ($('#label').length) {
								$('#label a').show().attr('href', '/label.php?groupId=' + $('#get-form input[name="group_id"]').val());
							}

							if ($('#trackingurl').length) {
								$('#trackingurl a').show().attr('href', response.getPackage.data[0].trackingurl);
							}
						}
					}

					if (response.getPackage.error_code == 0 && response.getPackage.type == 'success') {

						var packageData = response.getPackage.data[0];
						<?php if(getenv('TRACKING_SHOW_DATA') == null || getenv('TRACKING_SHOW_DATA') == 1){ ?>
							$('#sender-data, #consignee-data').html('');
							var view = '';
							view += '<li class="list-group-item">' + packageData.sender + '</li>';
							view += '<li class="list-group-item">' + packageData.sender_zip + ' ';
							view += '' + packageData.sender_city + ', ';
							view += '' + packageData.sender_address + ' ';
							view += '' + packageData.sender_apartment + '</li>';
							view += '<li class="list-group-item">' + packageData.sender_phone + '</li>';
							view += '<li class="list-group-item">' + packageData.sender_email + '</li>';
							$('#sender-data').append(view);

							view = '';
							view += '<li class="list-group-item">' + packageData.consignee + '</li>';
							view += '<li class="list-group-item">' + packageData.consignee_zip + ' ';
							view += '' + packageData.consignee_city + ', ';
							view += '' + packageData.consignee_address + ' ';
							view += '' + packageData.consignee_apartment + '</li>';
							view += '<li class="list-group-item">' + packageData.consignee_phone + '</li>';
							view += '<li class="list-group-item">' + packageData.consignee_email + '</li>';
							$('#consignee-data').append(view);
						<?php } ?>

						var packageItemsTableRows = '';
						$.each(packageData.packages, function(index, packageItem) {
							packageItemsTableRows += '<tr>';
							packageItemsTableRows += '<td>' + packageItem.weight + '</td>';
							packageItemsTableRows += '<td>' + packageItem.x + '</td>';
							packageItemsTableRows += '<td>' + packageItem.y + '</td>';
							packageItemsTableRows += '<td>' + packageItem.z + '</td>';
							packageItemsTableRows += '<td>' + packageItem.customcode + '</td>';
							packageItemsTableRows += '</tr>';
						});
						$('#package-items tbody').html(packageItemsTableRows);
					}
				});
			}

			if ($('input[name="group_id"').length && $('input[name="group_id"').val() != '') {
				getPackageData();
			}

			// csomag napló lekérdezés - űrlap küldés
			$('#get-form').on('submit', function(event) {
				event.preventDefault();
				$('#package-log-result').html('<li><i class="fas fa-spinner fa-spin fa-5x"></i></li>');

				$.ajax({
					method: "POST",
					url: "<?php echo url('/ajax.php?action=checkcaptcha'); ?>",
					data: {
						captcha: $('#captcha').val()
					},
					dataType: "json"
				}).done(function(response) {
					$('#package-log-result').html('');
					if ('error' in response) {
						alert(response.error);
					} else {
						getPackageData();
					}
				});

			});

			var priceCalcInputs = ['sender_zip', 'sender_city', 'sender_address', 'consignee_zip', 'consignee_city', 'consignee_address', 'customer_zip', 'customer_city', 'customer_address'];

			var selector_string_helper = [];

			$.each(priceCalcInputs, function(index, name) {
				selector_string_helper.push('input[name="' + name + '"]');
			});

			selector_string_helper.push('select[name="customer_country"]');
			selector_string_helper.push('select[name="delivery"]');
			selector_string_helper.push('input[name="insurance"]');
			selector_string_helper.push('input[name="referenceid"]');
			selector_string_helper.push('input[name="tracking"]');
			selector_string_helper.push('input[name="customer_data"]');
			selector_string_helper.push('input[name="cod"]');
			selector_string_helper.push('input[name="priority"]');
			selector_string_helper.push('input[name="saturday"]');

			var selector_string = selector_string_helper.join(',');

			//szolgáltalás díjának lekérdezése
			$(selector_string).change(function(){
				$.ajax({
					method: "POST",
					url: "<?php echo url('/ajax.php?action=getQuote'); ?>",
					data: $('#send-form').serialize(),
					dataType: "json"
				}).done(function(response) {
					if(response.type == "success"){
						$(".service_price h4").html(response.data.quote + " " + response.data.currency);
						$(".service_price .price_to_pay").val(response.data.quote);
						$(".service_price .currency").val(response.data.currency);
					} else {
						$(".service_price h4").html('???');
						$(".service_price .price_to_pay").val(0);
						$(".service_price .currency").val("HUF");
					}
				});

			});

			$("#add-package-row, .remove-item").click(function(){
				$.ajax({
					method: "POST",
					url: "<?php echo url('/ajax.php?action=getQuote'); ?>",
					data: $('#send-form').serialize(),
					dataType: "json"
				}).done(function(response) {
					if(response.type == "success"){
						$(".service_price h4").html(response.data.quote + " " + response.data.currency);
						$(".service_price .price_to_pay").val(response.data.quote);
						$(".service_price .currency").val(response.data.currency);
					} else {
						$(".service_price h4").html('???');
						$(".service_price .price_to_pay").val(0);
						$(".service_price .currency").val("HUF");
					}
				});

			});

			//tömeg számítása
			$('body').on('keyup', '.packages_weight', function() {
				calculateSummary();
			});
			$('body').on('change', '.packages_weight', function() {
				calculateSummary();
			});

			//térfogat számítása
			$('body').on('keyup', '.packages_x, .packages_y, .packages_z', function() {
				calculateSummary();
			});

			$('body').on('change', '.packages_x, .packages_y, .packages_z', function() {
				calculateSummary();
			});


		});
	</script>

	<?php if (getenv("PAYLIKE_PAYMENT")) {?>
		<script src="https://sdk.paylike.io/3.js"></script>

		<script>
			var paylike = Paylike('<?php echo getenv("PAYLIKE_KEY"); ?>');

			document.querySelector('.paylike_btn').addEventListener('click', pay);

			function pay(){

				//html 5 validation
				if (!$('#send-form')[0].checkValidity()) {
					$('#send-form').append('<input type="submit" class="temp_btn" />');
		            $('#send-form').find('input[type="submit"]').click();
					$('.temp_btn').remove();
		            return false;
		        }

				var confirmed = confirm('<?php echo getenv('MSG_CONFIRM_SEND'); ?>');

				if (confirmed) {
					var currency = $(".service_price .currency").val();
					var amount = ($(".service_price .price_to_pay").val()*100);

					if(amount < 1){
						$('#send-form').append('<input type="submit" class="temp_btn" />');
			            $('#send-form').find('input[type="submit"]').click();
						$('.temp_btn').remove();
						return true;
					}

					paylike.popup({
						currency: currency,
						amount: amount,
						title: "<?php echo Lang::instance()->_l('Csomagfeladás'); ?>",			// title text to show in popup
						locale: "<?php echo $_SESSION['lang']; ?>",			// pin the popup to a locale (e.g. en_US or en)
						fields: [
							{
								name: 'email',
								label: 'E-mail',	// same as `name` if not provided
								type: 'email',
								placeholder: 'user@example.com',
								required: true,
								value: $('input[name=customer_email]').val(),		// provide a default value
							},
							{
								name: 'sender',
								label: '<?php echo Lang::instance()->_l('Feladó'); ?>',	// same as `name` if not provided
								type: 'text',
								placeholder: '<?php echo Lang::instance()->_l('Feladó'); ?>',
								required: true,
								value: $('input[name=sender]').val(),		// provide a default value
							},
							{
								name: 'consignee',
								label: '<?php echo Lang::instance()->_l('Címzett'); ?>',	// same as `name` if not provided
								type: 'text',
								placeholder: '<?php echo Lang::instance()->_l('Címzett'); ?>',
								required: true,
								value: $('input[name=consignee]').val(),		// provide a default value
							}
						],
					}, function( err, r ){
						if (err){
							return console.warn(err);
						} else {
							console.log(r);	// { transaction: { id: ... } }
							//ha sikeres volt a fizetés, akkor feladjuk a csomagot
							$('#send-form').append('<input type="submit" class="temp_btn" />');
				            $('#send-form').find('input[type="submit"]').click();
							$('.temp_btn').remove();
							return true;
						}
					});
				}
			}
		</script>
	<?php }?>
</body>

</html>
