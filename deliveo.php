<?php

/**
 * Deliveo API réteg
 *
 * @author Jónás Zsolt
 */
class Deliveo {

    /**
     * GuzzleHttp client
     * 
     * @var object 
     */
    protected $client;

    public function __construct() {
        $this->client = new \GuzzleHttp\Client();
        $langCodes = Lang::instance()->getdeliveoApiLangCodes();
        $this->langCode = $langCodes[filter_var($_SESSION['lang'])];
    }

    /**
     * A kötelező mezők lekérdezésével ellenőrzi a kulcs érvényességét.
     * 
     * @return boolean
     */
    public function validKey() {
        $res = $this->client->request('GET', $this->url('required'));
        $response = json_decode($res->getBody());
        return $response->type === "success" && $response->error_code === 0;
    }

    /**
     * Kötelező mezők lekérése.
     * 
     * @return array
     */
    public function required() {
        $res = $this->client->request('GET', $this->url('required'));

        if ($res->getStatusCode() === 200) {
            $response = json_decode($res->getBody());

            $data = [];
            foreach ($response->data as $row) {
                $data[$row->value] = $row->description;
            }

            return $data;
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Szállítási opciók lekérése.
     * 
     * * @return array
     */
    public function getDelivery() {
        $res = $this->client->request('GET', $this->url('delivery'));

        if ($res->getStatusCode() === 200) {
            $response = json_decode($res->getBody());
            $options = [];
            foreach ($response->data as $row) {
                $options[$row->value] = $row->description;
            }
            return $options;
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Fuvardíjat fizeti opciók lekérdezése.
     * 
     * @return array
     */
    public function getFreight() {
        $res = $this->client->request('GET', $this->url('freight'));
        if ($res->getStatusCode() === 200) {
            $response = json_decode($res->getBody());

            $options = [];
            foreach ($response->data as $row) {
                $options[$row->value] = $row->description;
            }
            return $options;
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Csomag feladása.
     * 
     * @param array $postData
     * @return object
     */
    public function packageCreate($postData) {
        $res = $this->client->request('POST', $this->url('package/create'), ['form_params' => $postData]);
        if ($res->getStatusCode() === 200) {
            return json_decode($res->getBody());
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Csomag adatainak lekérése.
     * 
     * @param string $groupId Csoportkód
     * @return object
     */
    public function getPackage($groupId) {
        $res = $this->client->request('GET', $this->url('package/' . $groupId));
        if ($res->getStatusCode() === 200) {
            return json_decode($res->getBody());
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Napló lekérése.
     * 
     * Csomaghoz tartozó módosítások lekérése.
     * 
     * @param string $groupId Csoportkód
     * @return object
     */
    public function packageLog($groupId) {
        $res = $this->client->request('GET', $this->url('package_log/' . $groupId));

        if ($res->getStatusCode() === 200) {
            return json_decode($res->getBody());
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Ügyfél létrehozása
     * 
     * @param array $postData
     * @return object
     */
    public function customerCreate($postData) {
        $res = $this->client->request('POST', $this->url('customer/create'), ['form_params' => $postData]);
        if ($res->getStatusCode() === 200) {
            return json_decode($res->getBody());
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Csomagcímke url
     * 
     * @param string $groupId Csoportkód
     * @return object
     */
    public function labelUrl($groupId) {
        return $this->url('label/' . $groupId);
    }

    /**
     * Aláíráslap url
     * 
     * @param string $groupId Csoportkód
     * @return type
     */
    public function signatureUrl($groupId) {
        $res = $this->client->request('GET', $this->url('signature/' . $groupId));

        if ($res->getStatusCode() === 200) {
            $response = json_decode($res->getBody());
            
  
            if(is_object($response) && $response->type === 'error'){
                return false;
            }else{
                return $this->url('signature/' . $groupId);
            }
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
        
    }

    /**
     * Az api kérésekhez szükséges url-t állítja össze.
     * 
     * Pl.: http://deliveo.nanosoft.hu/api/package/v2/[PARANCS]?licence=LICENCE&api_key=API_KEY&lang=hu_HU
     * 
     * @param string $command PARANCS
     * @param array $parameters további opcionális paraméterek
     * @return string
     */
    private function url($command, $parameters = null) {
        
        $url = getenv('API_URL') . $command . '?licence=' . getenv('LICENCE') . '&api_key=' . getenv('API_KEY') . '&lang='.$this->langCode;

        if (!is_null($parameters) && is_array($parameters)) {
            foreach ($parameters as $parameterKey => $parameterValue) {
                $url .= '&' . $parameterKey . '=' . $parameterValue;
            }
        }

        return $url;
    }

}
