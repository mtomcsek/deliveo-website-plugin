<?php

 if(!extension_loaded('zip')){
     die('zip extension missing!');
 }
 if (version_compare(PHP_VERSION, '7.0') < 0) {
    die(PHP_VERSION . ' >= 7.0 php version required.');
}

//ini_set('display_errors', 1);
require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

$filesystem = new Filesystem();
 
$url = 'https://gitlab.com/mav-it/deliveo-website-plugin/-/archive/master/deliveo-website-plugin-master.zip';
$path = __DIR__ . '/temp.zip';
$file_path = fopen($path, 'a+');
$client = new \GuzzleHttp\Client();
$response = $client->get($url, ['sink' => $file_path]);
if ($response->getStatusCode() == 200) {
    $zip = new ZipArchive;
    $res = $zip->open('temp.zip');
    if ($res === TRUE) {
        $zip->extractTo(__DIR__ . '/');
        $zip->close();
        
        // remove zip
        $filesystem->remove([__DIR__ . '/temp.zip']);

        $except = ['.', '..', 'update', '.env','.git','vendor'];
        // remove 
        if ($handle = opendir(__DIR__ . '/..')) {
            while (false !== ($entry = readdir($handle))) {

                if (!in_array($entry, $except)) {
                     $filesystem->remove([__DIR__ . '/../'.$entry]);
                }
            }
            closedir($handle);
        }
        // update
        $except = ['.', '..', 'update', '.env','.git'];
        if ($handle = opendir(__DIR__ . '/deliveo-website-plugin-master')) {
            while (false !== ($entry = readdir($handle))) {

                if (!in_array($entry, $except)) {
    
                    if(is_dir(__DIR__ . '/deliveo-website-plugin-master/' . $entry)){
                        $filesystem->mirror(__DIR__ . '/deliveo-website-plugin-master/' . $entry, __DIR__ . '/../'. $entry);
                    }else{
                        $filesystem->copy(__DIR__ . '/deliveo-website-plugin-master/' . $entry,  __DIR__ . '/../'. $entry);
                    }
                }
            }
            closedir($handle);
        }
        
        $filesystem->remove([__DIR__ . '/deliveo-website-plugin-master']);
        
        echo 'Update was successful!';
        
    } else {
        $filesystem->remove([__DIR__ . '/temp.zip']);
        echo 'unzip failed!';
    }
}