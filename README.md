# Deliveo website plugin

Weboldalba építhető Deliveo plugin. Képes feladni csomagot és lekérdezni már feladott csomagok státuszát. 

**Csomag feladás:**

Megadható a feladó, a címzett, és a számlafizető külön is, de a számlafizető megegyezhet a feladóval vagy a címzettel. Szállítási opcióként azok az opciók jelennek meg, amelyek a Deliveo példányban léteznek.

**Lekérdezés:**

Írjuk be a csoportkódot a "Csoportkód" mezőbe, és az OK gomb megnyomásával lekérdezhetjük a csomag állapotát. (Ne felejtsük el beírni a Captchát.) Visszatérő adatként megkapjuk a feladó és a címzett adatait. Ha a keresés hozott találatot, akkor letölthetjük a csomaghoz kapcsolódó csomagcímkét is. Amennyiben a csomag már kézbesített (és be van kapcsolva a funkció) letölthetjük az aláíráslapot is, amin szerepel a kézbesítés helye és a címzett aláírása.

**Beállítások:**

A plugin többféle beállítást is lehetővé tesz. Az egyes beállítások a gyökérkönyvtárban elhelyezett .env fájlban módosíthatók.

Demó megtekinthető: https://dwp.nanosoft.hu/


# Deliveo website plugin

Deliveo plugin that can be built into a website. Able to drop a packet and query the status of packets already shipped.

**Package shipping:**

The sender, recipient, and bill payer can be specified separately, but the bill payer can be the same as the sender or recipient. The options that exist in the Deliveo instance are displayed as delivery options.

**Query:**

Enter the group code in the "Group code" field and press OK to query the status of the package. (Remember to enter the Captcha.) We return the sender and recipient information as return data. If the search returned a result, we can also download the package tag associated with the package. If the package has already been delivered (and the function is switched on), you can also download the signature sheet, which shows the place of delivery and the signature of the recipient.

**Settings:**

The plugin allows several settings. Each setting can be changed in an .env file located in the root directory.

Demo available at: https://dwp.nanosoft.hu/
